<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Help</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    
            <div class="main-content" style="background-color: #eee; padding-top: inherit; float: center; padding-right: 50px;padding-left: 50px;"><a href="index.php">KEMBALI</a>
                <br>
                <h3 align="center">Panduan Manual</h3>
                <br>
                <table cellspacing="0" cellpadding="0" border="1" width="100%">
                    <thead align="center">
                        <tr>
                            <th>1.Login Akses Admin Dan Operator</th>
                            <th>2.Login Akses Pegawai Atau User </th>
                            
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td>Untuk Masuk Ke Halaman Admin Dan Operator Yaitu :<br>
                                Admin => Username : FN99 <br>
                                         Password : Admin<br>
                                Operator => Username : Raja02<br>
                                            Password : Operator<br>

                            </td>
                            <td>Untuk Masuk Ke Halaman User Yaitu :<br>
                                User => Nip :123456 <br>
                                        Username : FN99 <br>
                                        Password : User123 <br>

                            </td>
                            
                        </tr>
                    </tbody>
                </table>
                <br>
                <table cellspacing="0" cellpadding="0" border="1" width="100%">
                    <thead align="center">
                        <tr>
                            <th>4.Halaman Admin</th>
                            <th>5.Halaman Operator</th>
                            <th>6.Halama User</th>
                            
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td>
                             Fitur Pada Admin : Inventaris,Peminjaman,Pengembalian,Generate Laporan,Master Data Dan Logout.<br>
                             <strong>Inventaris</strong> : yaitu data barang dan untuk mendata barang dan menambahkan data barang sesuai jurusan/tempat asal barang yg digunakan.<br> 
                             <strong>Peminjaman</strong> : yaitu admin sekaligus menambakan peminjaman bagi yang meminjam langsung kepada admin<br>  
                             <strong>Pengembalain</strong> : yaitu mengembalikan barang yangsudah dipinjam lalu mendatanya.<br> 
                             <strong>Generate Laporan</strong> : yaitu data detail barang peminjaman<br><strong>Master Data</strong> : yaitu data yg penting untuk melengkapi data bbarang inventris dan registrasi untuk user<br>
                             <strong>Logout</strong>  
                            </td>
                            <td>
                             Fitur Pada Operator : Peminjaman,Pengembalian dan Logout.<br>
                            
                             <strong>Peminjaman</strong> : yaitu operator sekaligus menambakan peminjaman bagi yang meminjam langsung kpada operator<br>  
                             <strong>Pengembalain</strong> : yaitu operator juga dapat mengembalikan barang yang sudah dipinjam lalu mendatanya.<br>
                             <strong>Logout</strong>  
                            </td>
                            <td>
                             Fitur Pada User :Peminjaman dan Logout.<br>
                             <strong>Peminjaman</strong> : yaitu user yang ingin meminjam barang dan mendaftar registrasi ke pada admin dan bisa melakukan peminjaman data sndiri<br>
                             <strong>Logout</strong>  
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
    

    
</body>

</html>

    <script src="js/main.js"></script>