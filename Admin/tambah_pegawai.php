<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Tambah Data USER </h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Tambah Pegawai / USER
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah_pg" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="kode_pegawai" placeholder="Kode Pegawai" autocomplete="off" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">NIP</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nip" placeholder="NIP" class="form-control" autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Lengkap Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_pegawai" placeholder="Nama Pegawai" autocomplete="off" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                       <label for="select" class=" form-control-label">Kelas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                          <select name="kelas" id="select" type="text" class="form-control">
                                                            <option>Kelas</option>
                                                            <option>10ANM1</option>
                                                            <option>10ANM2</option>
                                                            <option>11ANM1</option>
                                                            <option>11ANM2</option>
                                                            <option>12ANM1</option>
                                                            <option>12ANM2</option>
                                                            <option>10BC</option>
                                                            <option>11BC</option>
                                                            <option>12BC</option>
                                                            <option>10RPL1</option>
                                                            <option>10RPL2</option>
                                                            <option>10RPL3</option>
                                                            <option>11RPL1</option>
                                                            <option>11RPL2</option>
                                                            <option>11RPL3</option>
                                                            <option>12RPL1</option>
                                                            <option>12RPL2</option>
                                                            <option>12RPL3</option>
                                                            <option>10TKR1</option>
                                                            <option>10TKR2</option>
                                                            <option>11TKR1</option>
                                                            <option>11TKR2</option>
                                                            <option>12TKR1</option>
                                                            <option>12TKR2</option>
                                                            <option>10TPL</option>
                                                            <option>11TPL</option>
                                                            <option>12TPL</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Usernama</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="username" placeholder="Usernama" autocomplete="off" class="form-control" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="password" placeholder="Password" class="form-control" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Simpan" name="simpan">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
