<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Generate laporan</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                         <div class="row">
                                            <div class="col-md-6">
                                                Data Laporan Detail Pinjam
                                            </div>

                                            
                                       
                                         
                                        </div>
                                     
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th><center>No</center></th>
                                                                <th><center>Kode detail Peminjam</center></th>
                                                                <th><center>Kode Peminjaman</center></th>
                                                                <th><center>Kode Inventaris</center></th>
                                                                <th><center>Status</center></th>
                                                                <th><center>Jumlah</center></th>
                                                                <th><center>Opsi</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody align="center">
                                                                <?php
                                                                $no = 1 ;
                                                                foreach($db->peminjam() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $no++;?></td>
                                                                    <td><?php echo $x['kode_detail_pinjam'];?></td>
                                                                    <td><?php echo $x['kode_peminjaman'];?></td>
                                                                    <td><?php echo $x['kode_inventaris'];?></td>
                                                                    <td><?php echo $x['status_peminjaman'];?></td>
                                                                    <td><center><?php echo $x['jumlah'];?></center></td>
                                                                    <td width="40%">
                                                                    <center> 
                                                                        
                                                                        
                                                                        <a href="liat_detail.php?kode_peminjaman=<?php echo $x['kode_peminjaman'];?>&kode_inventaris=<?php echo $x['kode_inventaris'];?>">
                                                                        <button type="reset" class="btn btn-success btn-sm">
                                                                            <i class="fas fa-eye"></i> Detail
                                                                        </button>
                                                                        </a> 
                                                                        
                                                                        <a href="">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>
                                                                    </center>
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>

<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>