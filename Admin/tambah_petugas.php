<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Tambah Data Admin dan Operator</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Tambah Petugas
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah_pt" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Petugas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="kode_petugas" placeholder="Kode Petugas" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Petugas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_petugas" placeholder="Nama Petugas" class="form-control" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Username</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="username" placeholder="Username" class="form-control" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="password" placeholder="Password" class="form-control" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Level</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <select name="level" id="select" class="form-control" required>
                                                         <?php  
                                                        foreach ($db->level() as $level) {
                                                        ?>
                                                <option value="<?php echo $level['nama_level']; ?>" ><?php echo $level['nama_level']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Simpan">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
