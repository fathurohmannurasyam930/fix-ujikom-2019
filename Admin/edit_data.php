<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Data</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                         <?php
                                                    include"database/koneksi.php";
                                                    $kode_peminjaman=$_GET['kode_peminjaman'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM peminjaman WHERE kode_peminjaman='$kode_peminjaman'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                        <div class="card-header">
                                            <strong>Form</strong> Edit Data Kode Peminjaman : <?php echo $_GET['kode_peminjaman'];?>
                                            
                                        </div>
                                        <div class="card-body card-block">
                                           
                                            <form action="" method="post" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="hidden" value="<?php echo $_GET['kode_peminjaman'];?>" name="kode_peminjaman">
                                                        <input type="text" name="kode_inventaris" class="form-control" value="<?php echo $tampil['kode_inventaris'];?>" required >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Status Peminjaman</label>
                                                    </div>
                                                    <div class="col-12 col-md-4">
                                                        <input type="text" name="status_peminjaman" class="form-control" readonly value="<?php echo $tampil['status_peminjaman'];?>" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-4">
                                                        <input type="text" name="kode_pegawai" class="form-control" value="<?php echo $tampil['kode_pegawai'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kelas</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                        <input type="text" name="kelas" class="form-control" value="<?php echo $tampil['kelas'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" name="edit" value="edit">
                                                </div>
                                            </form>
                                           <?php
                                            include"Database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $kode_peminjaman=$_POST['kode_peminjaman'];
                                                $kode_inventaris=$_POST['kode_inventaris'];
                                                $status_peminjaman=$_POST['status_peminjaman'];
                                                $kode_pegawai=$_POST['kode_pegawai'];
                                                $kelas=$_POST['kelas'];
                                                $tanggal_pinjam=date("Y-m-d h:i:sa");

                                                $input=mysqli_query($koneksi, "UPDATE peminjaman SET kode_peminjaman='$kode_peminjaman', kode_inventaris='$kode_inventaris', status_peminjaman='$status_peminjaman', kode_pegawai='$kode_pegawai', kelas='$kelas', tanggal_pinjam='$tanggal_pinjam' WHERE kode_peminjaman='$kode_peminjaman'");
                                                $input=mysqli_query($koneksi ,"UPDATE inventaris SET jumlah = jumlah - 1 WHERE kode_inventaris='$kode_inventaris'");
                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="peminjaman.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>