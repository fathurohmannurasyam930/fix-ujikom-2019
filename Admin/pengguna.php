<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Data Pengguna</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                        <center>
                                     <h4>Data Petugas</h2>
                                     <br>
                                     <a href="tambah_petugas.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>Registrasi Petugas</button>
                                    </a>
                                    </center>
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead align="center">
                                                            <tr>
                                                                <th>NO</th>
                                                                <th>Kode Petugas</th>
                                                                <th>Nama Petugas</th>
                                                                <th>Level</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody align="center">
                                                                <?php
                                                                $no = 1;
                                                                foreach($db->petugas () as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $no++;?></td>
                                                                    <td><?php echo $x['kode_petugas'];?></td>
                                                                    <td><?php echo $x['nama_petugas'];?></td>
                                                                    <td><?php echo $x['level']; ?></td>
                                                                    <td>
                                                                        <a href="edit_petugas.php?kode_petugas=<?php echo $x['kode_petugas'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                         <a href="liat_petugas.php?kode_petugas=<?php echo $x['kode_petugas'];?>">
                                                                         <button type="submit" class="btn btn-success btn-sm">
                                                                            <i class="fas fa-eye"></i> Detail
                                                                        </button>
                                                                        </a>
                                                                        <a href="pro_inven.php?kode_petugas=<?php echo $x['kode_petugas']; ?>&aksi=hapus_pt">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>
                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                        <center>
                                     <h4>Data Pegawai</h2>
                                     <br>
                                     <a href="tambah_pegawai.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>Registrasi Pegawai</button>
                                    </a>
                                    </center>
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" class="table table-data2" id="dataTables2">
                                                        <thead>
                                                            <tr>
                                                                <th><center>No</center></th>
                                                                <th><center>NIP</center></th>
                                                                <th><center>Nama Pegawai</center></th>
                                                                <th><center>Kelas</center></th>
                                                                <th><center>Opsi</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody align="center">
                                                                <?php
                                                                $no = 1 ;
                                                                foreach($db->pegawai () as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $no++;?></td>
                                                                    <td><?php echo $x['nip']; ?></td>
                                                                <td><?php echo $x['nama_pegawai']; ?></td>
                                                                    <td><?php echo $x['kelas']; ?></td>
                                                                    
                                                                    <td>
                                                                        <a href="edit_pegawai.php?kode_pegawai=<?php echo $x['kode_pegawai'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                         <a href="liat_pegawai.php?kode_pegawai=<?php echo $x['kode_pegawai'];?>">
                                                                         <button type="submit" class="btn btn-success btn-sm">
                                                                            <i class="fas fa-eye"></i> Detail
                                                                        </button>
                                                                        </a>
                                                                        <a href="pro_inven.php?kode_pegawai=<?php echo $x['kode_pegawai']; ?>&aksi=hapus_pg">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                     
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>
 <script>
    $(document).ready(function() {
        $('#dataTables2').DataTable({
                responsive: true
        });
    });
 </script>