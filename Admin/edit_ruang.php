<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Ruang</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Edit Ruang
                                        </div>
                                        <div class="card-body card-block">
                                            <?php
                                                    include"database/koneksi.php";
                                                    $kode_ruang=$_GET['kode_ruang'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM ruang WHERE kode_ruang='$kode_ruang'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                            <form action="" method="post" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class="form-control-label">Kode Ruang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="hidden" name="kode_ruang" value="<?php echo $_GET['kode_ruang'];?>">
                                                    <input type="text" name="kode_ruang" class="form-control" value="<?php echo $tampil['kode_ruang'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Ruang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_ruang" class="form-control" value="<?php echo $tampil['nama_ruang'];?>" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="keterangan" class="form-control" value="<?php echo $tampil['keterangan'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" name="edit" value="edit">
                                                </div>
                                            </form>
                                           <?php
                                            include"database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $kode_ruang=$_POST['kode_ruang'];
                                                $nama_ruang=$_POST['nama_ruang'];
                                                $keterangan=$_POST['keterangan'];

                                                $input=mysqli_query($koneksi, "UPDATE ruang SET kode_ruang='$kode_ruang', nama_ruang='$nama_ruang', keterangan='$keterangan' WHERE kode_ruang='$kode_ruang'");

                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="ruang.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>